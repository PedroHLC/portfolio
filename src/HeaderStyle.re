open Emotion;
open AppStyle;

let navbar =
  css([
    backgroundColor(headerBg),
    padding(px(8)),
    color(headerText),
    select(
      "> div",
      [
        display(`flex),
        flexDirection(rowReverse),
        mobile([display(`block)]),
      ],
    ),
  ]);

let cookieMsg =
  css([
    flexGrow(1.0),
    mobile([marginBottom(px(8)), textAlign(`center)]),
  ]);
let langMenu =
  css([
    textAlign(`right),
    mobile([marginBottom(px(8)), textAlign(`center)]),
  ]);

let subNavbar =
  css([
    padding3(px(1), px(0), px(35)),
    backgroundColor(subNavbarBg),
    borderBottomColor(borderColor_),
    borderBottomWidth(px(1)),
    borderBottomStyle(solid),
    color(containerText),
    mobile([paddingBottom(px(0))]),
  ]);

let personalCard = css([]);

let cardTagKey =
  css(
    ~extend=button,
    [borderBottomRightRadius(px(0)), borderTopRightRadius(px(0))],
  );

let cardTagValue =
  css([
    backgroundColor(pageBg),
    border(px(1), solid, borderColor_),
    borderRadius(em(0.25)),
    borderTopLeftRadius(px(0)),
    borderBottomLeftRadius(px(0)),
    borderLeftWidth(px(0)),
    padding2(px(3), px(10)),
  ]);

let cardTag =
  css([
    listStyleType(`none),
    margin4(px(0), px(10), px(16), px(0)),
    display(inlineBlock),
    fontSize(px(11)),
  ]);

let cardTags =
  css([margin(px(0)), paddingLeft(px(0)), maxWidth(px(500))]);

let headerMenu =
  css([
    paddingLeft(px(0)),
    marginTop(px(-35)),
    select(
      "> li",
      [
        listStyleType(`none),
        display(inlineBlock),
        padding3(px(7), px(12), px(8)),
        height(px(17)),
        overflow(`hidden),
      ],
    ),
    mobile([
      backgroundColor(subNavbarBg),
      margin4(px(0), px(-8), px(0), px(-8)),
      padding2(px(8), px(0)),
      select(
        "> li",
        [display(block), margin4(px(0), px(8), px(0), px(8))],
      ),
    ]),
  ]);

let activeTab =
  css([
    border(px(1), solid, borderColor_),
    borderTop(px(3), solid, activeTabColor),
    borderTopLeftRadius(px(3)),
    borderTopRightRadius(px(3)),
    borderBottomWidth(px(0)),
    backgroundColor(pageBg),
  ]);