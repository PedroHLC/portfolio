open Global;

module Style = HeaderStyle;
module Theme = AppStyle;

let cardTag = ((key: string, value: React.element)) =>
  <li className=Style.cardTag>
    <strong className=Style.cardTagKey> {React.string(key)} </strong>
    <span className=Style.cardTagValue> value </span>
  </li>;

let personalCard = (fullName: string, tags: list((string, React.element))) => {
  <div className=Style.personalCard>
    <h1> {React.string(fullName)} </h1>
    {tags |> List.map(cardTag) |> ul(justClass(Style.cardTags))}
  </div>;
};

let pedroCard = (it: it) => {
  let since = it.headerCardSince;
  [
    (it.headerCardJob, React.string(since ++ " 2007")),
    ("Web", React.string("Full-Stack")),
    ("Android", React.string(since ++ " v2.2")),
    ("iOS", React.string("Swift")),
    ("Linux", React.string(since ++ " 2011")),
    ("Windows", React.string(".NET & COM")),
    (
      "E-mail",
      <a href="mailto:root@pedrohlc.com">
        Ico.mail
        {React.string("root@pedrohlc.com")}
      </a>,
    ),
    (
      "Curriculum Vitae",
      <a href="http://pedrohlc.com/Curriculum_Vitae.pdf" target="_blank">
        Ico.pdf
        {React.string("PDF")}
      </a>,
    ),
  ]
  |> personalCard(it.headerCardTitle);
};

let isMenuPage: sitePage => bool =
  page =>
    switch (page) {
    | FourOuFour => false
    | _ => true
    };

let pageIco = (page: sitePage) =>
  switch (page) {
  | Social => Ico.hashtag
  | About => Ico.me
  | Skills => Ico.book
  | Projects => Ico.paper
  | _ => Ico.lang
  };

let menuLink =
    (it: it, activePage: sitePage, setPage: sitePage => unit, page: sitePage) => {
  let pageLink = (link, _) => {
    setPage(link);
  };
  let domLink =
    <a onClick={pageLink(page)}>
      {pageIco(page)}
      {page |> Site.Page.getTitle(it) |> React.string}
    </a>;
  if (activePage == page) {
    <li className=Style.activeTab> domLink </li>;
  } else {
    <li> domLink </li>;
  };
};

let headerMenu = (it: it, setPage: sitePage => unit, activePage: sitePage) => {
  siteMap
  |> List.filter(isMenuPage)
  |> List.map(menuLink(it, activePage, setPage))
  |> ul(justClass(Style.headerMenu));
};

[@react.component]
let make =
    (
      ~it: it,
      ~site: siteModel,
      ~setLang: siteLang => unit,
      ~setPage: sitePage => unit,
    ) => {
  let setLang = (l, _) => setLang(l);
  <div>
    <div className=Style.navbar>
      <div className=Theme.container>
        <div className=Style.langMenu>
          Ico.lang
          <a onClick={setLang(English)}> {React.string("English")} </a>
          <span> {React.string(" / ")} </span>
          <a onClick={setLang(Portuguese)}>
            {React.string({j| Português |j})}
          </a>
        </div>
        <div className=Style.cookieMsg>
          <span> {React.string(it.cookieMsg)} </span>
        </div>
      </div>
    </div>
    <div className=Style.subNavbar>
      <div className=Theme.container> {pedroCard(it)} </div>
    </div>
    <div className=Theme.container>
      {headerMenu(it, setPage, site.page)}
    </div>
  </div>;
};