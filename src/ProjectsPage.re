open Global;
open ProjectsStyle;

module Theme = AppStyle;

let jobTag = React.string >> singleton >> li(None);

let jobBtn = ((url, title)) => {
  <a href=url target="_blank">
    <div className=jobLink> {React.string(title)} </div>
  </a>;
};

let jobBox =
    (
      imageURL: string,
      name: string,
      tags: list(string),
      time: string,
      desc: list(string),
      url: list((string, string)),
    ) => {
  let tagsUl = tags |> List.map(jobTag) |> ul(justClass(Theme.bodyTags));
  let img =
    switch (imageURL) {
    | "" => <span />
    | _ => <div className=jobImg> <img src=imageURL alt=name /> </div>
    }; // Maybe use option?
  <div>
    img
    <h4> {React.string(name)} </h4>
    tagsUl
    <li> {React.string("[ " ++ time ++ " ]")} </li>
    {desc |> List.map(parag(None) << singleton << React.string) |> div(None)}
    {url |> List.map(jobBtn) |> div(justClass(jobLinks))}
  </div>;
};

let job = (a, b, c, d, e, f) =>
  jobBox(a, b, c, d, e, f) |> singleton |> li(None);

let personal = {
  <div>
    <h3> {React.string("Personal and Open-Source")} </h3>
    <ul className=jobs>
      {job(
         "imgs/reason.svg",
         "This website",
         [
           "Personal",
           "SPA",
           "Reason",
           "React",
           "Emotion",
           "Webpack",
           "GitLab CI",
           "GitLab Pages",
         ],
         "2020 - Present.",
         [
           "When learning a new language, I usually start by creating a whole new project. Besides being a POC of the desired features, it's also an excellent way to anticipate the most severe challenges. That helps me get ready quicker and improve overall development quality.",
           "Computers from a development perspective change intensively. Every day there're new tools, libraries, and languages. Every situation has a different solution that requires a different combination of technologies. I long learned not to be attached nor afraid to discover and to anticipate what's the best combination to employ.",
           "That's precisely how this portfolio was born, out of necessity, curiosity, hype, and fun. It was my first ride with Reason, React, and GitLab Pages.",
         ],
         [
           ("https://portfolio.pedrohlc.com", "Permalink"),
           ("https://gitlab.com/PedroHLC/portfolio", "GitLab"),
         ],
       )}
      {job(
         "imgs/archlinux.png",
         "Chaotic-AUR",
         [
           "Open-Source Community",
           "ArchLinux",
           "Unofficial Repository",
           "cloud.ufscar.br",
           "Containers",
           "SystemD-NSpawn",
           "Shell script",
         ],
         "2018 - Present",
         [
           "Once, Dr. Matias asked me to do a billing system for UFSCar's cloud. This project never got out of the planning phase. However, while on it, I found the necessity to have a massive persistent project in which I could simulate and extract its measures. At the same time, I had multiple systems running ArchLinux, with a bunch of VCS packages that required recompiling on every update. I ended up asking dr. Matias, if I could make an UFSCar's unofficial repository.",
           "It follows ArchLinux's KISS philosophy, so it's simple but does what is supposed to. And without any dedicated advertisement, the server grew more than I expected. While I wrote this, it had 3200 users per month, two mirrors, and an Indian co-maintainer.",
           "It's a server that searches updates in more than 1700 projects hourly, builds them in personalized containers, packages, and persistently hosts the artifacts for ArchLinux's users.",
         ],
         [
           ("https://lonewolf.pedrohlc.com/chaotic-aur", "Homepage"),
           ("https://github.com/PedroHLC/chaotic-aur", "GitHub"),
         ],
       )}
      {job(
         "imgs/chaotic-installer.jpg",
         "Niemeyer",
         [
           "Open-Source Community",
           "ArchLinux",
           "Installer",
           "C++",
           "Qt5",
           "QML",
           "EGLFS",
           "Shell script",
           "Project Manager",
         ],
         "[Unfinished] 2019",
         [
           "I always wished I could automate parts of ArchLinux's installation process without losing the freedom to customize it manually. And that's this project is. It uses QT's EGLFS backend to render without X.Org or Wayland, and QML made everything easier.",
           "Focusing on usability by simplicity instead of complexity, I've advanced the project as I could in my free time. And when it was ready to receive help, I've documented the missing features made issues and roadmaps to future collaborators.",
           "I plan to deliver the first stable version during 2020's Hacktoberfest.",
         ],
         [("https://github.com/pedrohlc/niemeyer", "GitHub")],
       )}
      {job(
         "imgs/osdlyrics.jpg",
         "OSD Lyrics",
         ["Open-Source", "Project Manager", "C", "Python", "GTK", "LRC"],
         "2012 - Present",
         [
           "I'm not much proud of this item because I've mainly failed with it, but like every other failure, this one is also a lesson.",
           "It all started when I  reversed Android's MiniLyrics LRC search engine and published it in GitHub. Then I noticed I could port it to OSD Lyrics (a GTK widget for Linux's desktops that exhibits lyrics as karaoke for any player). OSD Lyrics' maintainer was absent at the moment, there were forks in the wild with fixes to small bugs, and it seemed there was no place to submit my patch. So I merged those forks into one. When the maintainer came back, he opened an official issue, where it became decided I could keep on his work.",
           "I shouldn't have accepted it! I could do patch reviewing, and I still do. I know neither of the two maintained versions (old in C and new partially in Python) codebase and never had the time to learn. Imagine solving the issues which appear till today. But I tried, and I've got some patches in, as well as collaborated a few.",
         ],
         [
           ("https://github.com/osdlyrics/osdlyrics", "GitHub"),
           ("https://twitter.com/OSDLyrics", "Twitter"),
           (
             "http://www.webupd8.org/2010/05/bring-lyrics-to-your-desktop-with-osd.html",
             "WebUpd8 post",
           ),
         ],
       )}
      {job(
         "imgs/ej5.jpg",
         "Serasa's Experiance Jam 5 #Blockchain",
         ["Hackaton", "Blockchain", "Python", "REST API", "Cryptography"],
         "Aug. 2018",
         [
           "I've got invited to my friends' team only one week before this Hackaton that was organized by Serasa. Also, they had already chosen one of the few themes available, Blockchains. Mr. Olivato, the team leader, knew the basics to self implement a blockchain. The challenge was: We had to present to Serasa a blockchain solution for storing client's information and sharing it with third parties when allowed.",
           "I was the one that coordinated how everything would work, where to use asymmetric encryption, how to combine that with symmetric, and the blockchain model. I also was the one that defined the REST API routes and parameters. I did of little coding too, and in the presentation, I was responsible for convincing my public that my code was (in technical means) the solution. We did all this in 30 hours.",
           "We didn't win. But we're one of the only two teams who got all problems covered and had a real working demo. We lost to the other, a solution reusing Etherium.",
         ],
         [
           (
             "https://github.com/PedroHLC/ej5-blockchain-equipe7",
             "Demo's GitHub",
           ),
           (
             "https://github.com/PedroHLC/ej5-blockchain-equipe7/raw/master/Cadastro%20Unico%20Digital(2).pdf",
             "Presentation's slides",
           ),
           (
             "https://medium.com/datalab-log/experiance-jam-341d2e176cda",
             "EJ5's Medium post",
           ),
         ],
       )}
      {job(
         "imgs/hackatruck.jpg",
         "Hackatruck",
         [
           "Course",
           "Eldorado",
           "IBM",
           "Swift",
           "iOS",
           "Mac/OSX",
           "Highly equipped bus",
           "Eye bee",
         ],
         "Aug. 2016",
         [
           "I've got the opportunity to participate in this Swift course in an IBM-sponsored bus highly equipped with Apple's products presented by Eldorado Research Institute. I've put it in this portfolio cause it felt way more like a Hackaton than a course.",
           "Each one of us participants got one MacBook and iPhone and organized in 4-people teams, challenged to come up and implement a daily use mobile app in Swift. I also helped many other groups with difficulties in programming.",
           "It was my first and only experience with iOS. However, I keep an OSX installation in a KVM-powered virtual machine on my computer, waiting for more.",
         ],
         [],
       )}
      {job(
         "",
         "Vala SDL2's bindings and cross-platform demo",
         [
           "GNOME",
           "Vala",
           "VAPI",
           "SDL2",
           "Andoid NDK",
           "OSX",
           "Linux",
           "Windows",
           "Port of XNA demo",
         ],
         "2013 - 2015",
         [
           "Though used primarily for games, SDL2 is a graphics framework and works for many other situations too. Today these SDL2's bindings are in the official GNOME's extra-VAPIs repository. Available to many distros and developers.",
           "I used to be an adventurer, and in one of those adventures, I've wanted to code a game and, without any modifications to my codebase, have it natively running on smartphones and desktops.",
           "GNOME had presented its very-new language, Vala, object-oriented that compiled into C with GLib. I saw that as the perfect opportunity as C compiles natively to pretty much anything.",
           "It wasn't easy at the time, but I prepared an Android NDK toolchain with all GLib dependencies, everything as ArchLinux's packages, many I had to write the PKGBUILDs. Also, there weren't any SDL2 bindings, so I had to write from scratch.",
           "And I've made it, I've got an XNA game making tutorial and ported it as a POC. My game did run on Linux, Windows, and Android, as well as I expected.",
         ],
         [
           (
             "https://gitlab.gnome.org/GNOME/vala-extra-vapis/-/blob/master/sdl2.vapi",
             "GNOME extra-VAPIs",
           ),
           (
             "http://manualgenie.blogspot.com/p/compilar-genie-vala-para-android.html",
             "My Android's effort acknowledged",
           ),
           (
             "https://www.dropbox.com/s/jh240gwi0ny4s2k/AvalancheShooterDemo.apk?dl=0",
             "Android's demo",
           ),
           (
             "https://github.com/avalanche-games/avalanche/tree/master/examples/shooter",
             "Demo's GitHub",
           ),
         ],
       )}
    </ul>
  </div>;
};

let neoArt = {
  <div>
    <h3> {React.string("Renato's NeoArt")} </h3>
    <ul className=jobs>
      {job(
         "imgs/heborn.jpeg",
         "HEBorn (HackerExperience 2)",
         [
           "Commercial Game",
           "Steam Greenlight",
           "Elm",
           "Jenkins CI/CD",
           "Webpack",
           "VirtualDOM",
           "Crowdfunding",
           "Phoenix Socket (w/ Elixir)",
           "Unit testing",
         ],
         "2017 - 2018",
         [
           "Working in this game was the most pleasant job experience I had. Renato had this crowdfunded game that was Steam greenlighted, a sequel to an indie hacker simulator.",
           "The codebase was terrific. Elm is quite captivating, it's pure, with no side-effects, so no runtime errors. It has a perfect looking syntax, and we adopted conventions that made everything look very readable. Code quality was the main effort of our team. Elixir was in the backend, and with Phoenix socket, both parts felt together perfectly. Renato coordinated CI/CD, applied bots to examine our merge requests, and everything was discussed and reviewed. We had an office but could also do remote work whenever it felt needed.",
           "After some time we noticed it wasn't such a good idea. We had focused too much on quality. The source code had too many files and small functions. That made code-readability incredible, but Elm's compiler was taking almost half an hour to compile one minor modification. We had unit tests that mostly asserted the compiler's functionality as runtime errors were impossible. And most important of all, we had a furious crowd that funded the game disappointed with the development rate. And they were right as we were far far away from the end. HEBorn was a POC that failed too much late in production. And eventually, in 2018, Renato decided to rewrite everything, the same time I left for focusing on my studies.",
           "Even with this failure, HEBorn taught me to love functional purity, a well-configured CI/CD, and colleagues that respect all quality development rules. The knowledge I've acquired using Elm's Virtual DOM has been quite useful as it's now mundane.",
         ],
         [
           ("https://github.com/HackerExperience/HEBorn", "GitHub"),
           (
             "https://steamcommunity.com/sharedfiles/filedetails/?id=669850719",
             "Steam Greenlight",
           ),
         ],
       )}
    </ul>
  </div>;
};

let ufscar = {
  <div>
    <h3> {React.string("UFSCar's Infra")} </h3>
    <ul className=jobs>
      {job(
         "imgs/sdvm.jpg",
         "SDVM",
         [
           "ufscar.br",
           "KVM",
           "Alpine Linux",
           "VFIO",
           "PCI Passthrough",
           "Eduroam",
           "802.11x",
           "qemu",
           "mkrootfs",
           "Dell's CCTK",
         ],
         "2019 - 2020",
         [
           "This project and paper are Mr. Lima's research, in which I participated since the planning till the production deployment. Mostly, as a consultant.",
           "Its primary objective consisted on to produce a host/guest solution capable of connecting to the Eduroam network (cable and wifi). For security reasons, one user's guest system has to be isolated from each other, and the user can't access the real network interface. It was interesting if the user could log in with a graphical interface, and the machine gave access to remote maintenance (including BIOS settings).",
           "We got that all solved: first, the login's graphical interface with Qt5 and EGLFS. We then made the automated immutable rootfs with AlpineLinux. Mr. Lima and Dr. Matias had already written the Eduroam DBus C++ Library. KVM and PCI passthrough made the very VM fast enough to play games. QCOW2's snapshots isolated users' images. Chaotic-AUR is used to generate an ArchLinux guest image.",
           "We made it so the login screen could reuse SDDM's greeters, and because of that, we ended up with the name SDVM.",
         ],
         [("http://pedrohlc.com/poster_marcos_1m.pdf", "Research's poster")],
       )}
      <li>
        {jobBox(
           "imgs/helios.png",
           "Helios",
           ["ufscar.br", "Python", "Django", "Postgres", "LDAP", "Docker"],
           "2018 - 2019",
           [
             "I've got to work with Helios (voting system) twice in UFSCar, both as a freelancer. My first job was to add support to use a list of LDAP user-ids to restrict voting users, making it suitable for the university usage. The second, to adapt it to reuse the docent's syndicate portal login, so through the portal, one could manage manual votes and block those users for voting twice.",
             "It's always quite interesting to get to the unknown land and make two different projects talk to each other and achieving a common goal.",
           ],
           [
             (
               "https://github.com/shirlei/helios-server/commits?author=PedroHLC",
               "Upstream commits",
             ),
           ],
         )}
        {jobBox(
           "imgs/google-play.png",
           "DC Autoconn",
           ["Personal", "dc.ufscar.br", "Android", "Captive portal"],
           "2017 - 2018",
           [
             "The computer department of UFSCar used to have a wifi network with a captive portal, that used a self-signed certificate for HTTPS.",
             "In Android, it was so complicated to log in at least once a day, always having to allow the certificate manually. It also wasn't safe.",
             "So I did this android app, which listens for wifi network changes. When SSID matches, it makes the post request to the captive portal, already expecting the correct certificate. I've published to Google Play, where it's till today.",
           ],
           [
             (
               "https://play.google.com/store/apps/details?id=com.pedrohlc.dcautoconn",
               "Play Store",
             ),
             (
               "https://bitbucket.org/PedroHLC/ufscardc_autoconn",
               "BitBucket",
             ),
           ],
         )}
      </li>
    </ul>
    <h3> {React.string("UFSCar's Computer Science course")} </h3>
    <ul className=jobs>
      <li>
        {jobBox(
           "",
           "ByteSurfer",
           ["Computer Science", "MASM", "x86_64", "Assembly", "Irvine"],
           "2018",
           [
             "This was the most exciting work I've done for school in my entire life. It was lovely learning x86_64's Assembly, MASM was sadly a requirement. Assembly was always a topic of interest, and I never imagined it was not that hard. I've only got to know JVM's Assembly before this, and it wasn't anything like this.",
             "As teamwork with my colleague Ms. Garcia, we've coded an AudioSurf clone for Window's virtual terminal. And all of it was in x86_64's Assembly with only Irvine and Window's old 32bit libraries to help us.",
           ],
           [("https://github.com/PedroHLC/ByteSurfer", "GitHub")],
         )}
        {jobBox(
           "",
           "Morphologic Treatment in Verilog",
           ["Computer Science", "Verilog", "ALTERA"],
           "2019",
           [
             "I've never imagined I would like to create my hardware. This course's project taught me otherwise. I had to write in Verilog some morphological treatments Filters. Like erosion and dilatation, achieving edge-detection, without undesired effects. Every step was visible on a VGA screen.",
             "I ended up doing more than the teacher requested. And I've recreated the classic DVD's screensaver using my image instead of the logo.",
           ],
           [
             (
               "https://github.com/PedroHLC/ufscar-sistemasdigitais-2019-2",
               "GitHub",
             ),
           ],
         )}
      </li>
      {job(
         "imgs/kimc.jpg",
         "KIM-C",
         [
           "Computer Science",
           "C",
           "CGI",
           "In-Filesystem DB",
           "Bootstrap CSS",
         ],
         "2016",
         [
           "For this course's project, I've needed to implement any game in C with CGI, which would run in a Linux server with only Apache installed.",
           "I've decided to implement a Mafia clone. Capable of holding multiple players and game rooms at once. There weren't any databases installed, so I had to make the users' sessions in another way. I've used the file system's folder as tables and rows, and file as cells. Wrote functions that created HTML (like a Virtual DOM), used C macros as it were C++ templates, and made filesizes trick to get only the newest chat events.",
           "It was exciting, but also very exhaustive to write it alone.",
         ],
         [
           ("https://bitbucket.org/PedroHLC/kimc", "BitBucket"),
           ("http://cap.dc.ufscar.br/~726578/", "Demo"),
         ],
       )}
    </ul>
    <h3> {React.string("SECOMP UFScar")} </h3>
    <ul className=jobs>
      {job(
         "imgs/secomp.png",
         "Full-Stack, DevOps and staff member",
         [
           "Event Organization",
           "Full-Stack",
           "Python",
           "Django 2",
           "PostgresSQL",
           "SQLite",
           "OverlayFS",
           "Debian",
           "ArchLinux",
           "DigitalOcean",
           "cloud.ufscar.br",
           "Android",
           "iOS",
         ],
         "Jan. - Nov. 2018",
         [
           "I've was a staff member in IX SECOMP UFScar, mostly as part of the IT team. It was very nice to be an organizer overall, and I've got to talk more with our guests than someone only participating. It's incredible to see an event you helped since planning to happen.",
           "We had a backend in Python's Django 2 that integrated with the event's website and mobile applications. Besides maintenance and small content updates, I've: ported it from SQLite to PostgresSQL, migrated from a Debian set in DigitalOcean to an immutable ArchLinux's in UFSCar's cloud using OverlayFS and a few GitLab APIs.",
         ],
         [
           (
             "https://web.archive.org/web/20181230083111/https://secompufscar.com.br/",
             "Website (2018 archive)",
           ),
         ],
       )}
    </ul>
    <h3> {React.string("ETEC's IT tecnician course")} </h3>
    <ul className=jobs>
      {job(
         "",
         "SQV & SQV4Droids",
         ["Scholarly monograph", "Java", "Android", "Desktop", "SQLite"],
         "2012",
         [
           "Every Brazilian that wants to enter a university first it must be approved in that university entrance test.",
           "When I was doing this course, the most popular Android version was Froyo, and I found it would be fantastic if I could prepare for these tests using my phone and desktop.",
           "So for the end of course's monograph, I've created a multi-platform app using Java that downloaded every question and allowed me to answer them randomly, keeping statistics data for tracking my progress.",
         ],
         [],
       )}
      {job(
         "",
         "Photoshop minicourse co-teacher",
         ["Teaching", "Photoshop", "Minicourse"],
         "2012",
         [
           "My school provided a three-day Photoshop minicourse open to the city's community, and I was one of the two teachers of one classroom. I've was one of two students invited to teach.",
           "Yeah, I've got some Photoshop's skills, can do most things both a photo editor and a designer does. Not as a primary skill, but an advantageous hobby. And teaching it was a pleasure!",
         ],
         [],
       )}
    </ul>
  </div>;
};

let divPropaganda = (it: it) => {
  let ph = (x, y) => {
    let imageURL = "imgs/" ++ x ++ ".jpg";
    <div>
      <div className=jobImg> <img src=imageURL alt=y /> </div>
      <a href=y target="_blank">
        <div className=jobLink> {React.string("Visit")} </div>
      </a>
    </div>;
  };
  <div>
    <h3> {it.projDIV |> React.string} </h3>
    <span> {it.projDIVDesc |> React.string} </span>
    <ul className=jobs>
      <li>
        {ph("expertcons", "http://expertcons.com.br/")}
        {ph("cmgadvocacia", "http://cmgadvocacia.com.br/novo/")}
        {ph("castrocalhas", "http://calhascastro.com.br")}
      </li>
      <li>
        {ph(
           "intercolor",
           "https://web.archive.org/web/20160110141229/http://intercolorbrasil.com.br/",
         )}
        {ph("costaevicente", "http://www.costaevicente.com.br/")}
      </li>
      <li> {ph("ilcabran", "http://www.ilcabran.com.br/")} </li>
    </ul>
  </div>;
};

[@react.component]
let make = (~it: it) => {
  <div>
    <h1> {it.outdated |> React.string} </h1>
    <h2> {it.pageProjectsTitle |> React.string} </h2>
    <span>
      {it.onlyEnglish |> React.string}
      {it.pageProjectsBody |> React.string}
    </span>
    personal
    neoArt
    ufscar
    {divPropaganda(it)}
    <h3> {React.string("Before 2013")} </h3>
    <span>
      {React.string(
         "I've had other projects before 2013, not exciting enough to make this page. Including a Multithreaded TCP C server with MySQL, real-time sound broadcasting from desktop to Android with UTP, IRC-like chats in Java, desktop VU-meter widget in C++, POCs of using Window's Aero theme in Delphi, image processing in C, some MSN Plus! Scripts, RPG Maker alternative encrypter, and a Minecraft hack that taught me JVM's Assembly.",
       )}
    </span>
    <br />
    <br />
  </div>;
};