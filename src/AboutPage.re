open Global;
open AboutStyle;

let meList = [
  (
    "Who am I?",
    [
      "Hi, I'm Pedro!",
      "I first started programming in 2007.",
      "I consider myself a DevOps, Full-Stack, and Mobile Developer, but my skillsets go further.",
      "I'm an open-source and functional programming enthusiast.",
    ],
  ),
  (
    "What am I like?",
    [
      "I've always been thrilled about understanding how things work, and this curiosity is what dragged me to computers.",
      "It all started with games development, then moved to servers/client apps, cryptography, bitmap transformations, web, mobile, FP, compilers, containers, Verilog, and it keeps going on.",
      "When first looking something new, I quickly notice everything that is wrong with it, everything that is missing, everything that could be better. And then end up imagining topic solutions for it all. And only after that, I solve the bigger problem, respecting the right balance between performance, quality, and simplicity.",
      "Consequently, I get many ideas on how to enhance our development environment.",
      "I'm not afraid of learning new languages, frameworks, or toolsets.",
    ],
  ),
  (
    "What do I know? Well, I know...",
    [
      "... a bunch of languages: Ruby, Python, C, Java, Javascript, C++, Rust, Pascal, Vala, Lua, Elm, R, Swift, Perl, VB, C#, PHP, Haskell, ReasonML, and hardware description with VHDL and SystemVerilog. Also, there is room for more!",
      "... how my computer works, from CPU components, assembly, BIOS, and bootloader, the kernel and the userspace, drivers, graphics, audio, and input stack, even the web rendering stack. I can say the same about my smartphone. This knowledge impacts a lot in the quality of what I code.",
      "... how to write servers with TCP, UDP, HTTP, WebSockets, and REST works. Web frameworks don't make it any more complicated.",
      "... how to replicate any given image in HTML and CSS, also putting all kinds of animation and making it responsive.",
      "... how to develop for Android, native, hybrid, and system native (NDK).",
      "... how to code Swift for iOS.",
      "... how to write unit and functional tests with any framework.",
      "I've studied English from 2005 until 2012.",
      "I've had commits reviewed and did the same for other team members.",
      "I've worked both remote and in-office before.",
    ],
  ),
];

[@react.component]
let make = (~it: it) => {
  let minor =
    React.string >> singleton >> parag(None) >> singleton >> li(None);
  let major = ((t, l)) =>
    <li>
      <h2> {t |> React.string} </h2>
      {List.map(minor, l) |> ul(None)}
    </li>;
  <div className=aboutMe>
    <span> {it.onlyEnglish |> React.string} </span>
    {meList |> List.map(major) |> ul(None)}
  </div>;
};