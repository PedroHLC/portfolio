let _dirty = (x, y) =>
  <span>
    <i className={"fa" ++ x ++ " fa-" ++ y} />
    {React.string(" ")}
  </span>;

let lang = _dirty("r", "comments");
let hashtag = _dirty("s", "hashtag");
let me = _dirty("s", "user-tie");
let book = _dirty("s", "book");
let paper = _dirty("r", "newspaper");
let pdf = _dirty("r", "file-pdf");
let mail = _dirty("r", "envelope");