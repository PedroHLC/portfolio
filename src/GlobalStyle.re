open Emotion;
open AppStyle;

global(
  "body",
  [
    minWidth(px(192)),
    fontFamily("Fira Sans, sans-serif"),
    margin(px(0)),
    fontSize(px(14)),
    backgroundColor(pageBg),
    color(bodyText),
  ],
);

let inheritColor = p("color", "inherit");

global(
  "a, a:hover, a:focus",
  [inheritColor, textDecoration(none), p("cursor", "pointer")],
);