/* FOR A BIGGER WEBSITE PREFER I18n */

type t = {
  pageNotFoundTitle: string,
  pageNotFoundBody: string,
  pageNotFoundClickHome: string,
  pageSocialTitle: string,
  pageSocialHeader: string,
  pageSocialBody: string,
  pageAboutTitle: string,
  pageSkillsTitle: string,
  pageProjectsTitle: string,
  pageProjectsBody: string,
  headerCardJob: string,
  headerCardSince: string,
  headerCardTitle: string,
  projDIV: string,
  projDIVDesc: string,
  cookieMsg: string,
  soonMsg: string,
  skillsDesc: string,
  onlyEnglish: string,
  outdated: string
};

let english: t = {
  pageNotFoundTitle: "Page not found",
  pageNotFoundBody: "The page you're looking for can't be found. Go home by ",
  pageNotFoundClickHome: "clicking here!",

  pageSocialTitle: "Links",
  pageSocialHeader: "Social Links",
  pageSocialBody: "These are professionally related online profiles where you'll also find me:",

  pageAboutTitle: "About me",

  pageSkillsTitle: "My skills",

  pageProjectsTitle: "My projects",
  pageProjectsBody: "These are projects I've collaborated in the last few years. They're personal, from the open-source community, freelances, or corporative jobs I've had the luck to be part.",

  headerCardJob: "Developer",
  headerCardSince: "since",
  headerCardTitle: "Campos, Pedro H. Lara",

  projDIV: "Websites at <DIV Propaganda />",
  projDIVDesc: "In 2013 - 2015 I worked as a Full-Stack developer at divpropaganda.com.br, where I've got the opportunity of working in more than 20 websites with a great team of designers and other colleagues. Some of these relics are still online, get a glimpse of them here:",

  cookieMsg: "This website use cookies to improve user experience.",
  soonMsg: "Sorry, I'm currently working on this tab.",

  skillsDesc: "Here's a list with some of my skillset:",
  onlyEnglish: "",

  outdated: "This portfolio has not been updated since 2020-03-30. For more up-to-date information, check my Curriculum Vitae in the PDF button above."
};

let portuguese: t = {
  pageNotFoundTitle: {j| Página não encontrada |j},
  pageNotFoundBody: {j| A página que você está procurando não pode ser encontrada. Volte para o início |j},
  pageNotFoundClickHome: "clicando aqui!",

  pageSocialTitle: "Links",
  pageSocialHeader: "Perfis Sociais",
  pageSocialBody: {j| Estes são links para redes sociais onde você pode encontrar mais do meu trabalho: |j},

  pageAboutTitle: "Sobre",

  pageSkillsTitle: "Habilidades",

  pageProjectsTitle: "Projetos",
  pageProjectsBody: {j| Estes são projetos que colaborei nos últimos anos. Entre eles projetos pessoais, da comunidade de código-aberto, freelances, ou corporativos que tive o prazer de participar.|j},

  headerCardJob: "Desenvolvedor",
  headerCardSince: "desde",
  headerCardTitle: "Pedro H. Lara Campos",

  projDIV: "Websites pela <DIV Propaganda />",
  projDIVDesc: {j| Em 2013 - 2015 trabalhei como desenvoledor full-stack na divpropaganda.com.br, onde tive a oportunidade de trabalhar em mais de 20 websites com uma excelente equipe de designers e outros colegas. Algumas dessas reliquias ainda estão online. |j},

  cookieMsg: {j| Este website usa cookies para melhorar sua navegação. |j},
  soonMsg: {j| Desculpa, ainda não terminei essa página! |j},

  skillsDesc: {j| Esta é uma lista com algumas das minhas habilidades: |j},
  onlyEnglish: {j| A Versão em Português dessa página não está pronta ainda! |j},

  outdated: {j| A lista de projetos dessa página não é atualizado desde 2020-03-30. Para informações mais atualizadas veja meu CV no botão de PDF acima. |j}
};