// Utils
let invoke: (unit => 'a) => 'a = fn => fn();
let uncurry2: (('a, 'b) => 'c, ('a, 'b)) => 'c = (f, (a, b)) => f(a, b);
let singleton: 'a => list('a) = x => [x];
let (>>): ('a => 'b, 'b => 'c, 'a) => 'c = (f, g, x) => g(f(x));
let (<<) = (f, g) => g >> f;

let listToTag:
  (string, option(ReactDOMRe.props), list(ReasonReact.reactElement)) =>
  React.element =
  (tag, props, children) =>
    children |> Array.of_list |> ReactDOMRe.createElement(tag, ~props?);

let ul = listToTag("ul");
let li = listToTag("li");
let parag = listToTag("p");
let div = listToTag("div");

let justClass = x => Some(ReactDOMRe.props(~className=x, ()));

[@bs.scope "navigator"] [@bs.val]
external browserLanguage: string = "language";

// Types
module Site = {
  module Lang = {
    type t =
      | English
      | Portuguese
      | French;

    let fromString = (s: string) => {
      let head = String.sub(s, 0, String.index(s, '-'));
      switch (head) {
      | "pt" => Portuguese
      | "fr" => French
      | _ => English
      };
    };

    let toString = self =>
      switch (self) {
      | Portuguese => "pt-BR"
      | French => "fr-FR"
      | English => "en-US"
      };

    let toISO639 = self =>
      switch (self) {
      | Portuguese => "pt"
      | French => "fr"
      | English => "en"
      };

    let setToDocument = self => {
      let setDocumentLang: string => unit = [%bs.raw
        {| function(x) { document.documentElement.lang =  x; } |}
      ];
      self |> toISO639 |> setDocumentLang;
    };

    let l = self => {
      switch (self) {
      | Portuguese => Internalization.portuguese
      | _ => Internalization.english
      };
    };
  };
  module Page = {
    type t =
      | Social
      | About
      | Skills
      | Projects
      | FourOuFour;
    let index = Projects;

    // Helpers
    let getHash: t => string =
      self =>
        switch (self) {
        | Social => "!wget"
        | About => "!whoami"
        | Skills => "!groups"
        | Projects => ""
        | FourOuFour => "!?"
        };

    let getLink = self => "#" ++ getHash(self);

    let getTitle = (l: Internalization.t, self) => {
      switch (self) {
      | Social => l.pageSocialTitle
      | About => l.pageAboutTitle
      | Skills => l.pageSkillsTitle
      | Projects => l.pageProjectsTitle
      | FourOuFour => l.pageNotFoundTitle
      };
    };

    let isHash = (hash: string, self) => getHash(self) == hash;

    let map = [Projects, Skills, About, Social, FourOuFour];

    let fromURL = (url: ReasonReact.Router.url) => {
      (map |> List.find_opt(isHash(url.hash)))
      ->Belt.Option.getWithDefault(FourOuFour);
    };
  };

  type t = {
    lang: Lang.t,
    page: Page.t,
  };
};

type siteModel = Site.t;
type sitePage = Site.Page.t;
type siteLang = Site.Lang.t;
type it = Internalization.t;
let siteMap = Site.Page.map;