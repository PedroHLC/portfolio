open Emotion;
open AppStyle;

let jobLink = css(~extend=button, [textAlign(center), margin(px(2))]);
let maxAsMax = p("max-width", "max-content");

let jobLinks =
  css([display(`flex), flexWrap(`wrap), select("> a", [flexGrow(1.0)])]);

let jobImg =
  css([
    margin(px(4)),
    textAlign(center),
    select("> img", [width(pct(100.0)), maxAsMax]),
  ]);

let jobs = css([select("> li", blockIt), ...blocks]);