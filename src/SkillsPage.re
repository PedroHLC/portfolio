open Global;

let skills = [
  "Android (SDK and NDK)",
  "Assembly IA-32 / JVM / MIPS",
  "Ash scripts / Dash / Bash (Shell)",
  "Bootstrap CSS",
  "C",
  "C++",
  "CertBot",
  "CGI",
  "CI/CD (GitLab, Jenkins)",
  "CMake",
  "Django",
  "Docker",
  "Elm",
  "Functional Programming",
  "Git / GitHub / GitLab",
  "GNU Make",
  "Gulp / Grunt",
  "Haskell",
  "HTML",
  "TCP / UDP / HTTP / REST",
  "Imperative programming",
  "Java",
  "Javascript",
  "LaTeX",
  "Linux / ArchLinux / Apline / Debian",
  "Markdown",
  "Mercurial",
  "NPM / Yarn",
  "Open-Source Projects",
  "OpenStack",
  "Pascal / Delphi",
  "PCI Passthrough",
  "Perl",
  "PHP",
  "Python",
  "QEMU / KVM / VFIO",
  "QtQuick / QML / Qt5",
  "R",
  "React / ReasonReact",
  "ReasonML",
  "Ruby / RubyOnRails",
  "SCSS / SASS / CSS",
  "SQL / MySQL / PostgreSQL / SQLite",
  "Swift",
  "SystemVerilog",
  "Unit / Functional testing",
  "Vala",
  "VB.NET",
  "Webpack",
  "Webservers (Apache, NGINX, handwritten)",
];

[@react.component]
let make = (~it: it) => {
  <div>
    <p> {React.string(it.skillsDesc)} </p>
    {skills
     |> List.map(
          React.string >> singleton >> div(None) >> singleton >> li(None),
        )
     |> div(justClass(SocialStyle.profiles))}
  </div>;
};