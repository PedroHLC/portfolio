open Global;
open SocialStyle;

module Theme = AppStyle;

let profile = (name: string, url: string) => {
  let readableURL = Js.String.replace("https://", "", url);
  <li>
    <div className=socialLink>
      <span> {React.string(name)} </span>
      <span>
        <a href=url target="_blank" className=Theme.button>
          {React.string(readableURL)}
        </a>
      </span>
    </div>
  </li>;
};

let gitHub = profile("GitHub", "https://github.com/PedroHLC");
let keyBase = profile("Keybase", "https://keybase.io/pedrohlc");
let gitLab = profile("GitLab", "https://gitlab.com/PedroHLC");
let bitBucket = profile("BitBucket", "https://bitbucket.org/PedroHLC");
let linkedIn = profile("LinkedIn", "https://linkedin.com/in/pedrohlc");
let angelCo = profile("AngelList", "https://angel.co/pedrohlc");

[@react.component]
let make = (~it: it) => {
  <div>
    <h2> {it.pageSocialTitle |> React.string} </h2>
    <span> {it.pageSocialBody |> React.string} </span>
    <ul className=profiles>
      gitHub
      keyBase
      gitLab
      bitBucket
      linkedIn
      angelCo
    </ul>
  </div>;
};