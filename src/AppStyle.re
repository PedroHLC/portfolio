open Emotion;

let mobile = media("(max-width: 436px)");

let containerWidth = 1012;
let containerSmaller =
  media("(max-width: " ++ Js.Int.toString(containerWidth - 1) ++ "px)");

let superPallete: array(Css.Color.t) =
  [|"1b262c", "0f4c75", "3282b8", "ebebff"|] |> Array.map(hex);

let textPallete: array(Css.Color.t) = [|
  hsla(0, 0, 0, 0.9),
  hsla(0, 0, 0, 0.7),
  hsla(0, 0, 100, 0.7),
|];

let pageBg = superPallete[3];
let headerBg = superPallete[0];
let subNavbarBg = superPallete[1];

let borderColor_ = superPallete[2];
let activeTabColor = superPallete[0];

let bodyText = textPallete[0];
let containerText = textPallete[1];
let headerText = textPallete[2];
let bodyTagText = textPallete[2];

let button =
  css([
    border(px(1), solid, borderColor_),
    borderRadius(em(0.25)),
    padding2(px(3), px(10)),
    backgroundImage(
      linearGradient(
        deg(-180.0),
        [(0, superPallete[3]), (90, superPallete[2])],
      ),
    ),
  ]);

let container =
  css([
    maxWidth(px(containerWidth)),
    marginLeft(auto),
    marginRight(auto),
    padding2(px(0), px(8)),
  ]);

let bodyTags =
  css([
    padding(px(0)),
    select(
      "> li",
      [
        listStyleType(`none),
        display(inlineBlock),
        backgroundColor(subNavbarBg),
        color(bodyTagText),
        borderRadius(em(0.25)),
        padding2(px(3), px(5)),
        margin4(px(0), px(4), px(2), px(0)),
        fontSize(px(10)),
        firstChild([backgroundColor(headerBg)]),
      ],
    ),
  ]);

let blocks = [
  display(`flex),
  flexWrap(`wrap),
  justifyContent(spaceAround),
  padding(px(0)),
  margin2(px(4), px(0)),
];

let blockIt = [
  flexGrow(1.0),
  flexShrink(1.0),
  flexBasis(pct(25.0)),
  //maxWidth(px(containerWidth / 3)),
  minWidth(px(containerWidth / 3)),
  listStyleType(`none),
  mobile([minWidth(pct(100.0)), maxWidth(`none)]),
  select(
    "> *",
    [
      display(`block),
      border(px(1), `solid, borderColor_),
      borderRadius(px(8)),
      margin(px(4)),
      padding(px(8)),
    ],
  ),
];