open Global;

[@react.component]
let make = (~setPage: sitePage => unit, ~it: it) => {
  let homeLink = _ => setPage(Site.Page.index);

  <div>
    <h1> {it.pageNotFoundTitle |> React.string} </h1>
    <span> {it.pageNotFoundBody |> React.string} </span>
    <a onClick=homeLink> {it.pageNotFoundClickHome |> React.string} </a>
  </div>;
};