open JustgageReasonCookie;
open Global;

module Theme = AppStyle;

// Model
type action =
  | SetLang(siteLang)
  | SetPage(sitePage)
  | RefreshPage(ReasonReact.Router.url);

// Init
let init: unit => siteModel =
  () => {
    let cookieLang = Cookie.getAsString("lang");

    let langInit =
      switch (cookieLang) {
      | Some(cookieLang) => Site.Lang.fromString(cookieLang)
      | _ =>
        if (browserLanguage != "") {
          Site.Lang.fromString(browserLanguage);
        } else {
          English;
        }
      };

    let browserURL = ReasonReact.Router.useUrl();
    let pageInit = Site.Page.fromURL(browserURL);

    {lang: langInit, page: pageInit};
  };

// Action
let reducer = (model: siteModel, action: action) => {
  switch (action) {
  | SetLang(lang) =>
    Cookie.setString("lang", Site.Lang.toString(lang));
    ignore(Site.Lang.setToDocument(lang));
    {...model, lang};
  | SetPage(page) =>
    ReasonReactRouter.push(Site.Page.getLink(page));
    {...model, page};
  | RefreshPage(url) => {...model, page: Site.Page.fromURL(url)}
  };
};

// Render
let getPageView: (sitePage => unit, it, siteModel, unit) => React.element =
  (setPage, it, site) =>
    switch (site.page) {
    | Social => (() => <SocialPage it />)
    | About => (() => <AboutPage it />)
    | Skills => (() => <SkillsPage it />)
    | Projects => (() => <ProjectsPage it />)
    | _ => (() => <NotFound it setPage />)
    };

[@react.component]
let make = () => {
  let (model, dispatch) = reducer->React.useReducer(init());

  let setLang = lang => dispatch(SetLang(lang));
  let setPage = page => dispatch(SetPage(page));
  let refreshPage = url => dispatch(RefreshPage(url));
  let it = Site.Lang.l(model.lang);

  ignore(ReasonReact.Router.watchUrl(refreshPage));
  ignore(Site.Lang.setToDocument(model.lang));

  <div>
    <HeaderComponent.make it site=model setLang setPage />
    <div className=Theme.container>
      {model |> getPageView(setPage, it) |> invoke}
    </div>
  </div>;
};