open Emotion;
open AppStyle;

let profileIt = [flexBasis(pct(25.0)), ...blockIt];

let profiles = css([select("> li", profileIt), ...blocks]);

let socialLink =
  css([
    important(display(`flex)),
    justifyContent(`spaceBetween),
    select("> span", [flexGrow(0.0)]),
  ]);