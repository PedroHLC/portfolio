const path = require('path');

module.exports = {
  entry: './src/index.bs.js',
  
  mode: 'development',
  output: {
    path: path.join(__dirname, "public"),
    filename: 'index.js',
  },

  optimization: {
    minimize: false,
  },
};